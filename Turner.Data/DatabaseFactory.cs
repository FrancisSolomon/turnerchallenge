// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="DatabaseFactory.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    using System;

    /// <summary>
    ///     The database factory.
    /// </summary>
    public class DatabaseFactory : IDatabaseFactory, IDisposable
    {
        #region Fields

        /// <summary>
        /// The database context.
        /// </summary>
        private TitlesContext databaseContext;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            if (this.databaseContext != null)
            {
                this.databaseContext.Dispose();
            }
        }

        /// <summary>
        ///     Gets the database context to be used for the Titles application.
        /// </summary>
        /// <returns>
        ///     The <see cref="TitlesContext" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public TitlesContext GetDatabaseContext()
        {
            return this.databaseContext ?? (this.databaseContext = new TitlesContext());
        }

        #endregion
    }
}