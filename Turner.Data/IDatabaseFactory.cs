﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="IDatabaseFactory.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    /// <summary>
    /// The DatabaseFactory interface.
    /// </summary>
    public interface IDatabaseFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the database context to be used for the Titles application.
        /// </summary>
        /// <returns>
        /// The <see cref="TitlesContext"/>.
        /// </returns>
        TitlesContext GetDatabaseContext();

        #endregion
    }
}