﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="ITitlesRepository.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    using System.Collections.Generic;

    using Turner.Models;

    /// <summary>
    /// The TitlesRepository interface.
    /// </summary>
    public interface ITitlesRepository
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets a title by ID.
        /// </summary>
        /// <param name="titleId">
        /// The ID to search for.
        /// </param>
        /// <returns>
        /// The <see cref="Title"/>.
        /// </returns>
        Title GetTitleById(int titleId);

        /// <summary>
        /// Gets a list of titles, filtered by the supplied string.
        /// </summary>
        /// <param name="titleFilter">
        /// The filter to use when retrieving titles.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<Title> GetTitles(string titleFilter);

        #endregion
    }
}