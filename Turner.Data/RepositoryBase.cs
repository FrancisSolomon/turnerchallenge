﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="RepositoryBase.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// The repository base.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class RepositoryBase<T>
        where T : class
    {
        #region Fields

        /// <summary>
        /// The db set.
        /// </summary>
        private readonly IDbSet<T> dbSet;

        /// <summary>
        /// The database factory.
        /// </summary>
        private IDatabaseFactory databaseFactory;

        /// <summary>
        /// The titles context.
        /// </summary>
        private TitlesContext titlesContext;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{T}"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
            this.titlesContext = databaseFactory.GetDatabaseContext();
            this.dbSet = this.titlesContext.Set<T>();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="whereExpression">
        /// The where expression.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        protected virtual IEnumerable<T> Get(Expression<Func<T, bool>> whereExpression)
        {
            return this.dbSet.Where(whereExpression);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        protected virtual T GetById(int id)
        {
            return this.dbSet.Find(id);
        }

        #endregion
    }
}