// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitlesContext.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// <summary>
//   The titles context.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    using System.Data.Entity;

    using Turner.Models;

    /// <summary>
    ///   The titles context.
    /// </summary>
    public class TitlesContext : DbContext
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TitlesContext" /> class.
        /// </summary>
        public TitlesContext()
            : base("name=TitlesContext")
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the awards.
        /// </summary>
        public virtual DbSet<Award> Awards { get; set; }

        /// <summary>
        ///     Gets or sets the genres.
        /// </summary>
        public virtual DbSet<Genre> Genres { get; set; }

        /// <summary>
        ///     Gets or sets the other names.
        /// </summary>
        public virtual DbSet<OtherName> OtherNames { get; set; }

        /// <summary>
        ///     Gets or sets the participants.
        /// </summary>
        public virtual DbSet<Participant> Participants { get; set; }

        /// <summary>
        ///     Gets or sets the story lines.
        /// </summary>
        public virtual DbSet<StoryLine> StoryLines { get; set; }

        /// <summary>
        ///     Gets or sets the title genres.
        /// </summary>
        public virtual DbSet<TitleGenre> TitleGenres { get; set; }

        /// <summary>
        ///     Gets or sets the title participants.
        /// </summary>
        public virtual DbSet<TitleParticipant> TitleParticipants { get; set; }

        /// <summary>
        ///     Gets or sets the titles.
        /// </summary>
        public virtual DbSet<Title> Titles { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .HasMany(e => e.TitleGenres)
                .WithRequired(e => e.Genre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Participant>()
                .HasMany(e => e.TitleParticipants)
                .WithRequired(e => e.Participant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Title>().HasMany(e => e.Awards).WithRequired(e => e.Title).WillCascadeOnDelete(false);

            modelBuilder.Entity<Title>()
                .HasMany(e => e.StoryLines)
                .WithRequired(e => e.Title)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Title>()
                .HasMany(e => e.TitleGenres)
                .WithRequired(e => e.Title)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Title>()
                .HasMany(e => e.TitleParticipants)
                .WithRequired(e => e.Title)
                .WillCascadeOnDelete(false);
        }

        #endregion
    }
}