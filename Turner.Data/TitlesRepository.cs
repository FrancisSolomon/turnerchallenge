﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitlesRepository.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Data
{
    using System.Collections.Generic;

    using Turner.Models;

    /// <summary>
    /// Returns title information from the database.
    /// </summary>
    public class TitlesRepository : RepositoryBase<Title>, ITitlesRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TitlesRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public TitlesRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets a title by ID.
        /// </summary>
        /// <param name="titleId">
        /// The ID to search for.
        /// </param>
        /// <returns>
        /// The <see cref="Title"/>.
        /// </returns>
        public Title GetTitleById(int titleId)
        {
            return this.GetById(titleId);
        }

        /// <summary>
        /// Gets a list of titles, filtered by the supplied string.
        /// </summary>
        /// <param name="titleFilter">
        /// The filter to use when retrieving titles.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Title> GetTitles(string titleFilter)
        {
            return this.Get(t => t.TitleName.Contains(titleFilter));
        }

        #endregion
    }
}