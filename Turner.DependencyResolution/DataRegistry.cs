// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="DataRegistry.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.DependencyResolution
{
    using StructureMap.Configuration.DSL;

    /// <summary>
    /// The data registry.
    /// </summary>
    public class DataRegistry : Registry
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRegistry"/> class.
        /// </summary>
        public DataRegistry()
        {
            this.Scan(
                x =>
                {
                    x.Assembly("Turner.Data");
                    x.WithDefaultConventions();
                });
        }

        #endregion
    }
}