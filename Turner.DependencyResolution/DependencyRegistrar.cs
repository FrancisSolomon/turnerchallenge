﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="DependencyRegistrar.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.DependencyResolution
{
    using StructureMap;

    /// <summary>
    /// The dependency registrar.
    /// </summary>
    public static class DependencyRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        /// Configures StructureMap. This method is run when the application starts.
        /// </summary>
        public static void ConfigureOnStartup()
        {
            ObjectFactory.Initialize(
                x =>
                {
                    x.AddRegistry<WebSiteRegistry>();
                    x.AddRegistry<ServicesRegistry>();
                    x.AddRegistry<DataRegistry>();
                });
        }

        #endregion
    }
}