// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="ServicesRegistry.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.DependencyResolution
{
    using StructureMap.Configuration.DSL;

    /// <summary>
    /// The services registry.
    /// </summary>
    public class ServicesRegistry : Registry
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesRegistry"/> class.
        /// </summary>
        public ServicesRegistry()
        {
            this.Scan(
                x =>
                {
                    x.Assembly("Turner.Services");
                    x.WithDefaultConventions();
                });
        }

        #endregion
    }
}