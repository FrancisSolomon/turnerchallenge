// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="WebSiteRegistry.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.DependencyResolution
{
    using StructureMap.Configuration.DSL;

    /// <summary>
    /// The web site registry.
    /// </summary>
    public class WebSiteRegistry : Registry
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebSiteRegistry"/> class.
        /// </summary>
        public WebSiteRegistry()
        {
            this.Scan(
                x =>
                {
                    x.Assembly("Turner.Web");
                    x.WithDefaultConventions();
                });
        }

        #endregion
    }
}