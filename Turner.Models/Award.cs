// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting LLC" file="Award.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing an award.
// </summary>
//
// --------------------------------------------------------------------------------------------------------------------

namespace Turner.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The award.
    /// </summary>
    [Table("Award")]
    public class Award
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the award 1.
        /// </summary>
        [Column("Award")]
        [StringLength(100)]
        public string Award1 { get; set; }

        /// <summary>
        /// Gets or sets the award company.
        /// </summary>
        [StringLength(100)]
        public string AwardCompany { get; set; }

        /// <summary>
        /// Gets or sets the award won.
        /// </summary>
        public bool? AwardWon { get; set; }

        /// <summary>
        /// Gets or sets the award year.
        /// </summary>
        public int? AwardYear { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual Title Title { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        #endregion
    }
}