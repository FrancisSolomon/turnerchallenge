// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="OtherName.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing other names by which the title is known.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The other name.
    /// </summary>
    [Table("OtherName")]
    public class OtherName
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual Title Title { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        public int? TitleId { get; set; }

        /// <summary>
        /// Gets or sets the title name.
        /// </summary>
        [StringLength(100)]
        public string TitleName { get; set; }

        /// <summary>
        /// Gets or sets the title name language.
        /// </summary>
        [StringLength(100)]
        public string TitleNameLanguage { get; set; }

        /// <summary>
        /// Gets or sets the title name sortable.
        /// </summary>
        [StringLength(100)]
        public string TitleNameSortable { get; set; }

        /// <summary>
        /// Gets or sets the title name type.
        /// </summary>
        [StringLength(100)]
        public string TitleNameType { get; set; }

        #endregion
    }
}