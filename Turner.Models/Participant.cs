// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="Participant.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing a participant in a title.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The participant.
    /// </summary>
    [Table("Participant")]
    public class Participant
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Participant"/> class.
        /// </summary>
        public Participant()
        {
            this.TitleParticipants = new HashSet<TitleParticipant>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the participant type.
        /// </summary>
        [StringLength(100)]
        public string ParticipantType { get; set; }

        /// <summary>
        /// Gets or sets the title participants.
        /// </summary>
        public virtual ICollection<TitleParticipant> TitleParticipants { get; set; }

        #endregion
    }
}