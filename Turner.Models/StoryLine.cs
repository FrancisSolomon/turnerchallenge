// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="StoryLine.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing the storyline of a title in a specific language.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The story line.
    /// </summary>
    [Table("StoryLine")]
    public class StoryLine
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        [StringLength(100)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual Title Title { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [StringLength(100)]
        public string Type { get; set; }

        #endregion
    }
}