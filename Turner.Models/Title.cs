// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="Title.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing a title.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The title.
    /// </summary>
    [Table("Title")]
    public class Title
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Title"/> class.
        /// </summary>
        public Title()
        {
            this.Awards = new HashSet<Award>();
            this.OtherNames = new HashSet<OtherName>();
            this.StoryLines = new HashSet<StoryLine>();
            this.TitleGenres = new HashSet<TitleGenre>();
            this.TitleParticipants = new HashSet<TitleParticipant>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the awards.
        /// </summary>
        public virtual ICollection<Award> Awards { get; set; }

        /// <summary>
        /// Gets or sets the other names.
        /// </summary>
        public virtual ICollection<OtherName> OtherNames { get; set; }

        /// <summary>
        /// Gets or sets the processed date time utc.
        /// </summary>
        public DateTime? ProcessedDateTimeUTC { get; set; }

        /// <summary>
        /// Gets or sets the release year.
        /// </summary>
        public int? ReleaseYear { get; set; }

        /// <summary>
        /// Gets or sets the story lines.
        /// </summary>
        public virtual ICollection<StoryLine> StoryLines { get; set; }

        /// <summary>
        /// Gets or sets the title genres.
        /// </summary>
        public virtual ICollection<TitleGenre> TitleGenres { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TitleId { get; set; }

        /// <summary>
        /// Gets or sets the title name.
        /// </summary>
        [StringLength(100)]
        public string TitleName { get; set; }

        /// <summary>
        /// Gets or sets the title name sortable.
        /// </summary>
        [StringLength(100)]
        public string TitleNameSortable { get; set; }

        /// <summary>
        /// Gets or sets the title participants.
        /// </summary>
        public virtual ICollection<TitleParticipant> TitleParticipants { get; set; }

        /// <summary>
        /// Gets or sets the title type id.
        /// </summary>
        public int? TitleTypeId { get; set; }

        #endregion
    }
}