// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitleGenre.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing a title-genre pair.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The title genre.
    /// </summary>
    [Table("TitleGenre")]
    public class TitleGenre
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        public virtual Genre Genre { get; set; }

        /// <summary>
        /// Gets or sets the genre id.
        /// </summary>
        public int GenreId { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual Title Title { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        #endregion
    }
}