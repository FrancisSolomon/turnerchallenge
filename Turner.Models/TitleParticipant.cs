// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitleParticipant.cs">
//   Copyright � 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
//
// <summary>
//   Model representing a title-participant pair.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The title participant.
    /// </summary>
    [Table("TitleParticipant")]
    public class TitleParticipant
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is key.
        /// </summary>
        public bool IsKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is on screen.
        /// </summary>
        public bool IsOnScreen { get; set; }

        /// <summary>
        /// Gets or sets the participant.
        /// </summary>
        public virtual Participant Participant { get; set; }

        /// <summary>
        /// Gets or sets the participant id.
        /// </summary>
        public int ParticipantId { get; set; }

        /// <summary>
        /// Gets or sets the role type.
        /// </summary>
        [StringLength(100)]
        public string RoleType { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual Title Title { get; set; }

        /// <summary>
        /// Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        #endregion
    }
}