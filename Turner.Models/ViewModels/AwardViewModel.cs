﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="AwardViewModel.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models.ViewModels
{
    /// <summary>
    /// The award view model.
    /// </summary>
    public class AwardViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the award.
        /// </summary>
        public string Award { get; set; }

        /// <summary>
        /// Gets or sets the award company.
        /// </summary>
        public string AwardCompany { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether award won.
        /// </summary>
        public bool AwardWon { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public int Year { get; set; }

        #endregion
    }
}