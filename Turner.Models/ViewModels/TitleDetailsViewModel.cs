﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitleDetailsViewModel.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models.ViewModels
{
    using System.Collections.Generic;

    /// <summary>
    ///     View model for the title details page.
    /// </summary>
    public class TitleDetailsViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the awards.
        /// </summary>
        public List<AwardViewModel> Awards { get; set; }

        /// <summary>
        /// Gets or sets the list of nominations.
        /// </summary>
        public List<AwardViewModel> Nominations { get; set; }

        /// <summary>
        ///     Gets or sets the genres.
        /// </summary>
        public List<string> Genres { get; set; }

        /// <summary>
        ///     Gets or sets the key actors.
        /// </summary>
        public List<ParticipantViewModel> KeyActors { get; set; }

        /// <summary>
        ///     Gets or sets the other actors.
        /// </summary>
        public List<ParticipantViewModel> OtherActors { get; set; }

        /// <summary>
        ///     Gets or sets the other names.
        /// </summary>
        public List<string> OtherNames { get; set; }

        /// <summary>
        ///     Gets or sets the other participants (director, etc.)
        /// </summary>
        public List<ParticipantViewModel> OtherParticipants { get; set; }

        /// <summary>
        ///     Gets or sets the story lines.
        /// </summary>
        public List<StoryLineViewModel> StoryLines { get; set; }

        /// <summary>
        ///     Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        /// <summary>
        ///     Gets or sets the title name.
        /// </summary>
        public string TitleName { get; set; }

        /// <summary>
        /// Gets or sets the release year.
        /// </summary>
        public int ReleaseYear { get; set; }

        #endregion
    }
}