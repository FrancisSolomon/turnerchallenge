﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitleViewModel.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models.ViewModels
{
    /// <summary>
    ///     The title view model.
    /// </summary>
    public class TitleViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the title id.
        /// </summary>
        public int TitleId { get; set; }

        /// <summary>
        ///     Gets or sets the title name.
        /// </summary>
        public string TitleName { get; set; }

        #endregion
    }
}