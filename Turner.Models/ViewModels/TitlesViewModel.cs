﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitlesViewModel.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Models.ViewModels
{
    using System.Collections.Generic;

    /// <summary>
    /// View model for the titles list.
    /// </summary>
    public class TitlesViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the list of titles.
        /// </summary>
        public IEnumerable<TitleViewModel> Titles { get; set; }

        #endregion
    }
}