﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="ITitlesService.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Services
{
    using System.Collections.Generic;

    using Turner.Models;
    using Turner.Models.ViewModels;

    /// <summary>
    /// The TitlesService interface.
    /// </summary>
    public interface ITitlesService
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets detailed information for a specified title ID.
        /// </summary>
        /// <param name="titleId">
        /// The ID of the title.
        /// </param>
        /// <returns>
        /// The detailed <see cref="Title"/> information.
        /// </returns>
        TitleDetailsViewModel GetTitleDetails(int titleId);

        /// <summary>
        /// Gets a list of titles filtered by the supplied string.
        /// </summary>
        /// <param name="titleFilter">
        /// The filter string to use.
        /// </param>
        /// <returns>
        /// The list of titles retrieved that match the filter.
        /// </returns>
        TitlesViewModel GetTitles(string titleFilter);

        #endregion
    }
}