﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="TitlesService.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Services
{
    using System.Linq;
    using System.Threading;

    using Turner.Data;
    using Turner.Models;
    using Turner.Models.ViewModels;

    /// <summary>
    /// The titles service.
    /// </summary>
    public class TitlesService : ITitlesService
    {
        #region Fields

        /// <summary>
        /// The titles repository.
        /// </summary>
        private readonly ITitlesRepository titlesRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TitlesService"/> class.
        /// </summary>
        /// <param name="titlesRepository">
        /// The titles repository.
        /// </param>
        public TitlesService(ITitlesRepository titlesRepository)
        {
            this.titlesRepository = titlesRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets detailed information for a specified title ID.
        /// </summary>
        /// <param name="titleId">
        /// The ID of the title.
        /// </param>
        /// <returns>
        /// The detailed <see cref="Title"/> information.
        /// </returns>
        public TitleDetailsViewModel GetTitleDetails(int titleId)
        {
            var title = this.titlesRepository.GetTitleById(titleId);
            var textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

            if (title == null)
            {
                return null;
            }

            var vm = new TitleDetailsViewModel
            {
                TitleId = title.TitleId,
                TitleName = title.TitleName,
                ReleaseYear = title.ReleaseYear.GetValueOrDefault(1900),
                StoryLines =
                    title.StoryLines.Select(x => new StoryLineViewModel { Description = x.Description, Language = x.Language, StorylineType = x.Type }).ToList(),
                OtherNames = title.OtherNames.Select(x => string.Format("{0} ({1})", x.TitleName, textInfo.ToTitleCase(x.TitleNameLanguage))).ToList(),
                Awards = title.Awards.Where(x => x.AwardWon.GetValueOrDefault(false)).Select(x => new AwardViewModel
                {
                    Award = x.Award1,
                    AwardCompany = x.AwardCompany,
                    AwardWon = x.AwardWon.GetValueOrDefault(false),
                    Year = x.AwardYear.GetValueOrDefault(1900)
                }).ToList(),
                Nominations = title.Awards.Where(x => x.AwardWon.GetValueOrDefault(false) == false).Select(x => new AwardViewModel
                {
                    Award = x.Award1,
                    AwardCompany = x.AwardCompany,
                    AwardWon = x.AwardWon.GetValueOrDefault(false),
                    Year = x.AwardYear.GetValueOrDefault(1900)
                }).ToList(),
                Genres = title.TitleGenres.Select(x => x.Genre.Name).ToList(),
                KeyActors = title.TitleParticipants.Where(x => x.IsKey && x.RoleType == "Actor").Select(x => new ParticipantViewModel
                {
                    Name = x.Participant.Name
                }).ToList(),
                OtherActors = title.TitleParticipants.Where(x => x.IsKey == false && x.RoleType == "Actor").Select(x => new ParticipantViewModel
                {
                    Name = x.Participant.Name
                }).ToList(),
                OtherParticipants = title.TitleParticipants.Where(x => x.RoleType != "Actor").Select(x => new ParticipantViewModel
                {
                    Name = x.Participant.Name,
                    Type = x.RoleType
                }).ToList()
            };
            return vm;
        }

        /// <summary>
        /// Gets a list of titles filtered by the supplied string.
        /// </summary>
        /// <param name="titleFilter">
        /// The filter string to use.
        /// </param>
        /// <returns>
        /// The list of titles retrieved that match the filter.
        /// </returns>
        public TitlesViewModel GetTitles(string titleFilter)
        {
            var titles = this.titlesRepository.GetTitles(titleFilter);
            var vm = new TitlesViewModel
            {
                Titles = titles.OrderBy(x => x.TitleNameSortable).Select(x => new TitleViewModel { TitleId = x.TitleId, TitleName = x.TitleName })
            };

            return vm;
        }

        #endregion
    }
}