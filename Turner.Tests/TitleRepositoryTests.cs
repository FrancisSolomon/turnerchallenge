﻿namespace Turner.Tests
{
    using System.Linq;

    using NUnit.Framework;

    using Turner.Data;

    [TestFixture]
    public class TitleRepositoryTests
    {
        private IDatabaseFactory databaseFactory;

        private TitlesRepository titlesRepository;

        [TestFixtureSetUp]
        public void SetupTestFixture()
        {
            this.databaseFactory = new DatabaseFactory();
        }

        [SetUp]
        public void Setup()
        {
            this.titlesRepository = new TitlesRepository(this.databaseFactory);
        }

        [Test]
        public void GetByIdWithValidIdShouldReturnATitle()
        {
            // Arrange

            // Act
            var result = this.titlesRepository.GetTitleById(610);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.TitleId, Is.EqualTo(610));
        }

        [Test]
        public void GetByIdWithInvalidIdShouldNotReturnATitle()
        {
            // Arrange

            // Act
            var result = this.titlesRepository.GetTitleById(611);

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetWithNoFilterShouldReturnAllTitles()
        {
            // Arrange

            // Act
            var result = this.titlesRepository.GetTitles(string.Empty).ToList();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(25));
            // ReSharper disable PossibleMultipleEnumeration
            Assert.That(result.FirstOrDefault(x => x.TitleId == 610), Is.Not.Null);
            Assert.That(result.FirstOrDefault(x => x.TitleId == 341476), Is.Not.Null);
            // ReSharper restore PossibleMultipleEnumeration
        }

        [Test]
        public void GetWithFilterShouldReturnCorrectTitles()
        {
            // Arrange

            // Act
            var result = this.titlesRepository.GetTitles("All").ToList();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(5));
            // ReSharper disable PossibleMultipleEnumeration
            Assert.That(result.FirstOrDefault(x => x.TitleId == 12708), Is.Not.Null);
            Assert.That(result.FirstOrDefault(x => x.TitleId == 16636), Is.Not.Null);
            Assert.That(result.FirstOrDefault(x => x.TitleId == 27628), Is.Not.Null);
            Assert.That(result.FirstOrDefault(x => x.TitleId == 67044), Is.Not.Null);
            Assert.That(result.FirstOrDefault(x => x.TitleId == 67079), Is.Not.Null);
            // ReSharper restore PossibleMultipleEnumeration
        }

        [Test]
        public void GetWithFilterMatchingNoRecordsShouldReturnEmptyResultset()
        {
            // Arrange

            // Act

            // Assert
            var result = this.titlesRepository.GetTitles("Blort").ToList();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(0));
        }
    }
}
