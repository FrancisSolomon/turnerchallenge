﻿namespace Turner.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Moq;

    using NUnit.Framework;

    using Turner.Data;
    using Turner.Models;
    using Turner.Services;

    [TestFixture]
    public class TitleServiceTests
    {
        private TitlesService titlesService;

        private Mock<ITitlesRepository> titlesRepository;

        [SetUp]
        public void SetupTest()
        {
            this.titlesRepository = new Mock<ITitlesRepository>();
            this.titlesService = new TitlesService(titlesRepository.Object);
        }

        [Test]
        public void GetTitleByIdWithValidIdShouldReturnCorrectInformation()
        {
            // Arrange
            var titleToReturn = new Title { TitleId = 1, TitleName = "The Rocky Horror Picture Show" };
            this.titlesRepository.Setup(x => x.GetTitleById(1)).Returns(titleToReturn);

            // Act
            var returnedTitle = this.titlesService.GetTitleDetails(1);

            // Assert
            this.titlesRepository.Verify(x => x.GetTitleById(1), Times.Once);
            Assert.That(returnedTitle.TitleId, Is.EqualTo(titleToReturn.TitleId));
            Assert.That(returnedTitle.TitleName, Is.EqualTo(titleToReturn.TitleName));
        }

        [Test]
        public void GetTitleByIdWithInvalidIdShouldNotReturnARecord()
        {
            // Arrange
            this.titlesRepository.Setup(x => x.GetTitleById(It.IsAny<int>())).Returns(null as Title);

            // Act
            var returnedTitle = this.titlesService.GetTitleDetails(1);

            // Assert
            this.titlesRepository.Verify(x => x.GetTitleById(1), Times.Once);
            Assert.That(returnedTitle, Is.Null);
        }

        [Test]
        public void GetTitlesWithNoFilterShouldReturnAllTitles()
        {
            // Arrange
            var titlesToReturn = new List<Title>
            {
                new Title { TitleId = 1, TitleName = "Ankle Biters", ProcessedDateTimeUTC = DateTime.UtcNow },
                new Title { TitleId = 2, TitleName = "Xanadu", ProcessedDateTimeUTC = DateTime.UtcNow }
            };
            this.titlesRepository.Setup(x => x.GetTitles(string.Empty)).Returns(titlesToReturn);

            // Act
            var returnedTitles = this.titlesService.GetTitles(string.Empty);

            // Assert
            this.titlesRepository.Verify(x => x.GetTitles(string.Empty), Times.Once);

            Assert.That(returnedTitles, Is.Not.Null);
            Assert.That(returnedTitles.Titles, Is.Not.Null);
            Assert.That(returnedTitles.Titles.Count(), Is.EqualTo(2));
            Assert.That(returnedTitles.Titles.First().TitleId, Is.EqualTo(1));
            Assert.That(returnedTitles.Titles.Last().TitleId, Is.EqualTo(2));
        }

        [Test]
        public void GetTitlesShouldReturnCorrectInformationInCorrectOrder()
        {
            // **NOTE** this is NOT testing the filtering functionality, because that is not part of the service.

            // Arrange
            var titlesToReturn = new List<Title>
            {
                new Title { TitleId = 1, TitleName = "Iron Monkey", TitleNameSortable = "Iron Monkey", ProcessedDateTimeUTC = DateTime.UtcNow },
                new Title { TitleId = 2, TitleName = "Iron Man", TitleNameSortable = "Iron Man", ProcessedDateTimeUTC = DateTime.UtcNow },
            };
            this.titlesRepository.Setup(x => x.GetTitles(It.IsAny<string>())).Returns(titlesToReturn);

            // Act
            var returnedTitles = this.titlesService.GetTitles("Iron");

            // Assert
            this.titlesRepository.Verify(x => x.GetTitles("Iron"), Times.Once);

            Assert.That(returnedTitles, Is.Not.Null);
            Assert.That(returnedTitles.Titles, Is.Not.Null);
            Assert.That(returnedTitles.Titles.Count(), Is.EqualTo(2));

            var firstTitle = returnedTitles.Titles.First();
            Assert.That(firstTitle, Is.Not.Null);
            // ReSharper disable once PossibleNullReferenceException
            Assert.That(firstTitle.TitleId, Is.EqualTo(2));
            Assert.That(firstTitle.TitleName, Is.EqualTo("Iron Man"));

            var secondTitle = returnedTitles.Titles.Last();
            Assert.That(secondTitle, Is.Not.Null);
            // ReSharper disable once PossibleNullReferenceException
            Assert.That(secondTitle.TitleId, Is.EqualTo(1));
            Assert.That(secondTitle.TitleName, Is.EqualTo("Iron Monkey"));
        }
    }
}
