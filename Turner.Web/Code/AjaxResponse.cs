﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="AjaxResponse.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Web.Code
{
    /// <summary>
    ///     This class should be used as the return type (converted to JSON) for all ajax responses
    /// </summary>
    public class AjaxResponse
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the redirect url.
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// Gets or sets the response data.
        /// </summary>
        public object ResponseData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Creates a "success" reponse template which can be customized before being sent to the client.
        /// </summary>
        /// <param name="responseData">
        /// The response data to be sent.
        /// </param>
        /// <returns>
        /// The <see cref="AjaxResponse"/>.
        /// </returns>
        public static AjaxResponse CreateSuccessfulResponse(object responseData)
        {
            return new AjaxResponse { Success = true, ResponseData = responseData };
        }

        #endregion
    }
}