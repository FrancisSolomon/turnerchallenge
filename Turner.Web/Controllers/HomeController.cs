﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="HomeController.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Web.Controllers
{
    using System;
    using System.Web.Mvc;

    using Turner.Services;
    using Turner.Web.Code;

    /// <summary>
    /// Controller for the home/root page
    /// </summary>
    public class HomeController : Controller
    {
        #region Fields

        /// <summary>
        /// The titles service.
        /// </summary>
        private readonly ITitlesService titlesService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="titlesService">
        /// The titles service.
        /// </param>
        public HomeController(ITitlesService titlesService)
        {
            this.titlesService = titlesService;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Handles the default route for this controller.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet]
        public ActionResult Index()
        {
            var titles = this.titlesService.GetTitles(string.Empty);
            return View(titles);
        }

        /// <summary>
        /// Handles an AJAX post with a filter string.
        /// </summary>
        /// <param name="filterString">
        /// The filter string.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Index(string filterString)
        {
            var result = new AjaxResponse();

            try
            {
                result.ResponseData = this.titlesService.GetTitles(filterString);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.ErrorMessage = ex.Message;
            }

            return this.Json(result);
        }

        /// <summary>
        /// Handles an AJAX request to retrieve details for an individual title
        /// </summary>
        /// <param name="titleId">
        /// The ID of the title to be retrieved.
        /// </param>
        /// <returns>
        /// A <see cref="JsonResult"/> containing the title details and status information.
        /// </returns>
        [HttpPost]
        [AjaxOnly]
        public JsonResult Title(int titleId = -1)
        {
            var result = new AjaxResponse();

            try
            {
                result.ResponseData = this.titlesService.GetTitleDetails(titleId);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.ErrorMessage = ex.Message;
            }

            return this.Json(result);
        }

        #endregion
    }
}