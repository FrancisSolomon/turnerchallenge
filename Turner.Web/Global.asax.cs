﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="Global.asax.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Web
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using Turner.DependencyResolution;

    /// <summary>
    /// Represents the web application.
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        #region Methods

        /// <summary>
        /// Runs when a new request to the application starts.
        /// </summary>
        protected void Application_BeginRequest()
        {
            // This tells Internet Explorer not to use compatibility mode
            this.Response.AddHeader("X-UA-Compatible", "IE=edge");
        }

        /// <summary>
        /// Runs when the application starts.
        /// </summary>
        protected void Application_Start()
        {
            // Configure StructureMap
            DependencyRegistrar.ConfigureOnStartup();
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        #endregion
    }
}