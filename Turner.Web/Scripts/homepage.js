﻿$(function () {
    var homePageManager = new HomePageManager();
    homePageManager.initialize();
});

function HomePageManager() {
    this.initialize = function() {
        // Set up search button handler.
        $('#buttonSearch').on('click', function () {
            $.ajax({
                type: "POST",
                url: baseUrl + "Home/Index",
                data: JSON.stringify({
                    filterString: $('#inputSearch').val()
                }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
            }).done(function (data) {
                var titles = data.ResponseData.Titles;
                var tableBody = $('#titlesTableBody');
                var rowCount = titles.length;
                tableBody.empty();

                for (var i = 0; i < rowCount; i++) {
                    var title = titles[i];
                    var newRow = $('<tr><td class="movie-title" data-movieid="' + title.TitleId + '">' + title.TitleName + '</td></tr>');
                    tableBody.append(newRow);
                }
            }).fail(function(xhr, status, error) {
                $('#errorDlgText').text(error);
                $('#errorDlg').modal();
            });
        });

        // Set up title click handler.
        $('#titlesTableBody').on('click', 'tr', function () {
            console.log('this: ', this, $(this));
            console.log('attr: ', $(this).children('td').first().attr('data-movieid'));

            $.ajax({
                type: 'POST',
                url: baseUrl + 'Home/Title',
                data: JSON.stringify({
                    titleId: $(this).children('td').first().attr('data-movieid')
                }),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
            }).done(function (data) {
                // Populate the title details information in the dialog
                var title = data.ResponseData;
                var i;
                var newRow;
                var output = [];
                $('#movieDetailsLabel').text(title.TitleName + ' (' + title.ReleaseYear + ')');
                $('#genres').text(title.Genres.join(', '));
                $('#othernames').text(title.OtherNames.join(', '));

                var storylineTab = $('#storyline');
                storylineTab.empty();

                for (i = 0; i < title.StoryLines.length; i++) {
                    var storyline = title.StoryLines[i];
                    var storylineInfo = $('<h4>' + storyline.Language + ' (' + storyline.StorylineType + ')</h4><p>' + storyline.Description + '</p>');
                    storylineTab.append(storylineInfo);
                }

                for (i = 0; i < title.KeyActors.length; i++) {
                    output.push(title.KeyActors[i].Name);
                }

                $('#mainactors').text(output.join(', '));

                output = [];

                for (i = 0; i < title.OtherActors.length; i++) {
                    output.push(title.OtherActors[i].Name);
                }

                $('#otheractors').text(output.join(', '));
                var crewTable = $('#crewlist');
                crewTable.empty();

                for (i = 0; i < title.OtherParticipants.length; i++) {
                    var participant = title.OtherParticipants[i];
                    newRow = $('<tr><td>' + participant.Name + '</td><td>' + participant.Type + '</td></tr>');
                    crewTable.append(newRow);
                }

                var awardTable = $('#awardlist');
                awardTable.empty();

                for (i = 0; i < title.Awards.length; i++) {
                    var award = title.Awards[i];
                    newRow = $('<tr><td>' + award.Year + '</td><td>' + award.AwardCompany + ' for ' + award.Award + '</td></tr>');
                    awardTable.append(newRow);
                }

                var nominationTable = $('#nominationlist');
                nominationTable.empty();

                for (i = 0; i < title.Nominations.length; i++) {
                    var nomination = title.Nominations[i];
                    newRow = $('<tr><td>' + nomination.Year + '</td><td>' + nomination.AwardCompany + ' for ' + nomination.Award + '</td></tr>');
                    nominationTable.append(newRow);
                }


                $('#movieDetails').modal();
            }).fail(function (xhr, status, error) {
                $('#errorDlgText').text(error);
                $('#errorDlg').modal();
            });
        });

    };
}