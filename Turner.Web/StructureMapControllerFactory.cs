﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="S2 Consulting, LLC" file="StructureMapControllerFactory.cs">
//   Copyright © 2014, S2 Consulting LLC. All rights reserved.
// </copyright>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace Turner.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using StructureMap;

    /// <summary>
    /// The structure map controller factory.
    /// </summary>
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        #region Methods

        /// <summary>
        /// Gets a controller instance from StructureMap.
        /// </summary>
        /// <param name="requestContext">
        /// The request context.
        /// </param>
        /// <param name="controllerType">
        /// The controller type.
        /// </param>
        /// <returns>
        /// A <see cref="IController"/> instantiated by StructureMap.
        /// </returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (requestContext == null)
            {
                return null;
            }

            if (controllerType == null)
            {
                return base.GetControllerInstance(requestContext, null);
            }

            var controller = ObjectFactory.GetInstance(controllerType) as Controller;
            return controller;
        }

        #endregion
    }
}